package garden.bots.unimatrix

import org.graalvm.polyglot.*

class Kompilo {
  var language: String = ""
  val scriptContext = Context.newBuilder().allowAllAccess(true).build()

  fun compileFunction(functionCode : String, language: String) : Result<Any> {
    this.language = language
    return try {
      Result.success(scriptContext.eval(Source.create(language, functionCode)))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }

  fun invokeFunction(name: String?, params: Any) : Result<Any> {
    return try {
      Result.success(scriptContext.getBindings(this.language).getMember(name).execute(params))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }
}
