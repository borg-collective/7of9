package garden.bots.unimatrix

import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.ext.healthchecks.HealthCheckHandler
import io.vertx.ext.healthchecks.Status
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.servicediscovery.Record
import io.vertx.servicediscovery.ServiceDiscovery
import io.vertx.servicediscovery.ServiceDiscoveryOptions
import io.vertx.servicediscovery.rest.ServiceDiscoveryRestEndpoint
import io.vertx.servicediscovery.types.HttpEndpoint


class MainVerticle : AbstractVerticle() {
  private lateinit var discovery: ServiceDiscovery
  private lateinit var record: Record

  override fun stop(stopPromise: Promise<Void>) {
    // === Discovery ===
    val redisHost = System.getenv("REDIS_HOST") // "redis-mother.database"

    if(!redisHost.isNullOrEmpty()) {
      println("Unregistration process is started (${record.registration})...")
      discovery.unpublish(record.registration) { ar ->
        when {
          ar.failed() -> {
            println("😡 Unable to unpublish the microservice: ${ar.cause().message}")
            stopPromise.fail(ar.cause())
          }
          ar.succeeded() -> {
            println("👋 bye bye ${record.registration}")
            stopPromise.complete()
          }
        }
      }
    }
    // === End of Discovery ===
  }

  override fun start(startPromise: Promise<Void>) {
    // Prometheus counter
    var function_call_success_counter = 0
    var function_call_error_counter = 0

    // Health check handler
    val healthCheck = HealthCheckHandler.create(vertx)

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())
    val kompilo = Kompilo()
    val language = System.getenv("LANG") ?: "js"
    val httpPort = System.getenv("PORT")?.toInt() ?: 8080
    val readme = System.getenv("README") ?: "👋 Hello World 🌍"
    val contentType = System.getenv("CONTENT_TYPE") ?: "application/json;charset=UTF-8"
    val functionName = System.getenv("FUNCTION_NAME") ?: "hello"

    // === Discovery ===
    val redisHost = System.getenv("REDIS_HOST") // "redis-mother.database"

    if(!redisHost.isNullOrEmpty()) {

      val redisPort= System.getenv("REDIS_PORT")?.toInt() ?: 6379
      val redisAuth = System.getenv("REDIS_PASSWORD") ?: null
      val redisRecordsKey = System.getenv("REDIS_RECORDS_KEY") ?: "vert.x.ms" // the redis hash
      val serviceDiscoveryOptions = ServiceDiscoveryOptions()

      discovery = ServiceDiscovery.create(vertx,
        serviceDiscoveryOptions.setBackendConfiguration(
          json {
            obj(
              "host" to redisHost,
              "port" to redisPort,
              "auth" to redisAuth,
              "key" to redisRecordsKey
            )
          }
        ))

        // create the microservice record
        val serviceName = functionName
        val serviceHost = System.getenv("SERVICE_HOST") ?: "john-doe-service.127.0.0.1.nip.io"
        val serviceExternalPort= System.getenv("SERVICE_PORT")?.toInt() ?: 80

        record = HttpEndpoint.createRecord(
          functionName,
          serviceHost, // or internal ip
          serviceExternalPort, // exposed port (internally it's 8080)
          "/"
        )

        // add metadata
        record.metadata = json {
        obj(
          "description" to "7of9 service",
          "health-check" to "/health",
          "http-discovery" to "/discovery",
          "function-name" to functionName
        )
      }
        // Creates the REST endpoint using the default root (/discovery).
        ServiceDiscoveryRestEndpoint.create(router, discovery)

      discovery.publish(record) { asyncRes ->
        when {
          asyncRes.failed() ->
            println("😡 Not able to publish the microservice: ${asyncRes.cause().message}")
          asyncRes.succeeded() -> {
            println("😃 Microservice is published! ${asyncRes.result().registration}")
          }
        }
      }
    }
    // === End of Discovery ===


    val functionCode = System.getenv("FUNCTION_CODE") ?: """
      function ${functionName}(params) {
        return {
          message: "👋 Hello World 🌍",
        }
      }
    """.trimIndent()

    val compiledFunction = kompilo.compileFunction(functionCode, language)

    //router.route("/*").handler(StaticHandler.create().setCachingEnabled(false))

    compiledFunction.let {
      when {
        it.isFailure -> { // compilation error

          healthCheck.register("compilation") { promise ->
              promise.complete(Status.KO())
          }

          router.post("/").handler { context ->
            context.response().putHeader("content-type", "application/json;charset=UTF-8")
              .end(
                json {
                  obj("compilationError" to it.exceptionOrNull()?.message)
                }.encodePrettily()
              )

          }
        }

        it.isSuccess -> { // compilation is OK, and name of the function to invoke is "handle"
          //val function = it.b

          healthCheck.register("compilation") { promise ->
            promise.complete(Status.OK())
          }

          router.post("/").handler { context ->

            //val params = context.bodyAsJson.encodePrettily()
            val params = context.bodyAsJson

            // call the function
            kompilo.invokeFunction(functionName, params).let {
              when {
                it.isFailure -> { // execution error
                  function_call_error_counter +=1
                  context.response().putHeader("content-type", "application/json;charset=UTF-8")
                    .end(
                      json {
                        obj("ExecutionError" to it.exceptionOrNull()?.message)
                      }.encodePrettily()
                    )
                }
                it.isSuccess -> { // execution is OK
                  function_call_success_counter +=1
                  val result = it.getOrNull()
                  context.response().putHeader("content-type", contentType)
                    .end(result.toString())
                }
                else -> { TODO() }
              }
            }
          }
        }
        else -> { TODO() }
      }
    }

    // link/bind healthCheck to a route
    router.get("/health").handler(healthCheck)

    // only for test
    router.get("/check").handler { context ->
      context.response().putHeader("content-type", "text/plain;charset=UTF-8")
        .end("🍊🍋🍅")
    }

    router.get("/").handler { context ->
      context.response().putHeader("content-type", "text/plain;charset=UTF-8")
        .end(readme)
    }

    // Prometheus
    router.get("/metrics").handler { context ->
      val results = """
            # HELP error counter.
            # TYPE error gauge
            error $function_call_error_counter
            # HELP success counter.
            # TYPE success gauge
            success $function_call_success_counter
        """.trimIndent()

      context.response().putHeader("content-type", "text/plain;charset=UTF-8").end(results)
    }

    vertx
      .createHttpServer()
      .requestHandler(router)
      .listen(httpPort) { http ->
        when {
          http.failed() -> {
            startPromise.fail(http.cause())
          }
          http.succeeded() -> {
            println("🤖 GraalVM 7of9 runtime for $functionName function started on port $httpPort")
            startPromise.complete()
          }
        }
      }
  }
}
