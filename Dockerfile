# build the application
FROM maven:3.6.0-jdk-8-alpine AS builder
COPY . .
RUN mvn clean package

# oracle graalvm
#FROM oracle/graalvm-ce:20.0.0
# debian slim graalvm
FROM findepi/graalvm:java8
RUN set -xeu && \
    gu install python && \
    gu install ruby
# RUN gu install wasm (TODO)
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
COPY --from=builder target/*-fat.jar /app.jar
# run the application
CMD [ "java", "-Dfile.encoding=UTF8", "-jar", "/app.jar" ]
